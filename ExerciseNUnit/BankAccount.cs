﻿namespace ProjectExercise;

public class BankAccount
{
    private double _interestRate;
    private string _owner;
    private double _balance;

    public BankAccount(string owner, double balance, double interestRate)
    {
        if (owner == string.Empty)
        {
            _owner = "Denys";
        }
        else
        {
            _owner = owner;
        }
        _balance = balance < 0.0 ? 0.0 : balance;
        _interestRate = interestRate < 0.0 ? 0.0 : interestRate;
    }

    public double Balance
    {
        get
        {
            return _balance;
        }
    }

    public double InterestRate => _interestRate;
    public string Owner => _owner;

    public void Withdraw(double amount)
    {
        if (amount > 0.0 && _balance > 0.0)
        {
            _balance -= amount;
        }
    }

    public void Deposit(double amount)
    {
        if (amount > 0.0 && _balance > 0.0)
        {
            _balance += amount;
        }
    }

    public void AddInterests()
    {
        if (_balance > 0.0)
        {
            _balance += _balance * _interestRate;
        }
    }

    public override string ToString()
    {
        return $"owner {_owner} has {_balance} balance with interest {_interestRate}";
    }
}