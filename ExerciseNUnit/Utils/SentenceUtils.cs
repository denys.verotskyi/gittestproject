﻿using System.Text;

namespace ProjectExercise.Utils
{
    /*
        A sentence with words separated by spaces. Function returns a copy of this sentence where all the words:
        - start with a capital letter
        - the rest of the letters are lower case
        - all leading and trailing whitespace characters are trimmed
        Example:
        " This is SO  MUCH  FUN! " => "This Is So  Much  Fun!"
     */
    public class SentenceUtils
    {
        public static string ToTitleCase(string sentence)
        {
            if (sentence == null || sentence.Length == 1)
            {
                return string.Empty;
            }

            StringBuilder result = new StringBuilder(sentence.Trim());
            result[0] = char.ToUpper(result[0]);
            for (int i = 1; i < result.Length; i++)
            {
                result[i] = char.IsWhiteSpace(result[i - 1]) ? char.ToUpper(result[i]) : char.ToLower(result[i]);
            }

            return result.ToString();
        }
    }
}