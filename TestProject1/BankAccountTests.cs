﻿using NUnit.Framework;
using ProjectExercise;

namespace TestProject1
{
    [TestFixture]
    public class BankAccountTests
    {
        [Test]
        public void ConstructorEmptyOwner()
        {
            //arrange
            string owner = string.Empty;
            string expectedOwner = "Denys";

            //act
            BankAccount bankAccount = new BankAccount(owner, 0.0, 0.0);

            //assert
            string actualOwner = bankAccount.Owner;
            Assert.That(actualOwner, Is.EqualTo(expectedOwner));
        }

        [Test]
        public void DepositPositiveAmountWithPositiveBalance()
        {
            //arrange
            double amount = 100.0;
            BankAccount bankAccount = new BankAccount("Anna", 50.0, 0.0);

            //act
            bankAccount.Deposit(amount);

            //assert
            double expectedBalance = 150.0;
            double actualBalance = bankAccount.Balance;
            Assert.That(actualBalance, Is.EqualTo(expectedBalance));
        }
    }
}
