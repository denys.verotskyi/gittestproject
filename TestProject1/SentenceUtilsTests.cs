using ProjectExercise.Utils;
using NUnit.Framework;

namespace TestProject1
{
    [TestFixture]
    public class SentenceUtilsTests
    {
        [Test]
        public void AllLettersSmallCase()
        {
            //arrange
            string sentence = "denys daria sasha anna";
            string expectedResult = "Denys Daria Sasha Anna";

            //act
            string actualResult = SentenceUtils.ToTitleCase(sentence);
            
            //assert
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }
    }
}